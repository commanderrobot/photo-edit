//
//  CLEEffect.m
//  PhotoEdit
//
//  Created by Christian Lee on 28/09/13.
//  Copyright (c) 2013 Clee. All rights reserved.
//

#import "CLEEffect.h"

@implementation CLEEffect

- (id)initWithFilter:(GPUImageOutput<GPUImageInput>*)filter name:(NSString*)name minX:(CGFloat)minX maxX:(CGFloat)maxX
{
	self = [super init];
	
	if (self)
	{
		self.name = name;
		
		self.filterOne = filter;
		self.minX = minX;
		self.maxX = maxX;
		
		self.minY = 0;
		self.maxY = 0;
	}
	
	return self;
}

- (id)initWithFilter:(GPUImageOutput<GPUImageInput>*)filter name:(NSString*)name minX:(CGFloat)minX maxX:(CGFloat)maxX minY:(CGFloat)minY maxY:(CGFloat)maxY
{
	self = [super init];
	
	if (self)
	{
		self.name = name;
		
		self.filterOne = filter;
		self.minX = minX;
		self.maxX = maxX;
		
		self.minY = minY;
		self.maxY= maxY;
	}
	
	return self;
}

- (id)initWithFilterOne:(GPUImageOutput<GPUImageInput>*)filterOne filterTwo:(GPUImageOutput<GPUImageInput>*)filterTwo name:(NSString*)name minX:(CGFloat)minX maxX:(CGFloat)maxX minY:(CGFloat)minY maxY:(CGFloat)maxY
{
	self = [super init];
	
	if (self)
	{
		self.name = name;
		
		self.filterOne = filterOne;
		self.minX = minX;
		self.maxX = maxX;
		
		self.filterTwo = filterTwo;
		self.minY = minY;
		self.maxY= maxY;
	}
	
	return self;
}

@end
