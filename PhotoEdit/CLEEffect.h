//
//  CLEEffect.h
//  PhotoEdit
//
//  Created by Christian Lee on 28/09/13.
//  Copyright (c) 2013 Clee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GPUImageFilter.h"

@protocol GPUImageInput;

@interface CLEEffect : NSObject

@property (strong) NSString *name;
@property (strong) GPUImageOutput<GPUImageInput> *filterOne;
@property (strong) GPUImageOutput<GPUImageInput> *filterTwo;
@property (assign) CGFloat minX;
@property (assign) CGFloat maxX;
@property (assign) CGFloat minY;
@property (assign) CGFloat maxY;

- (id)initWithFilter:(GPUImageOutput<GPUImageInput>*)filter name:(NSString*)name minX:(CGFloat)minX maxX:(CGFloat)maxX;
- (id)initWithFilter:(GPUImageOutput<GPUImageInput>*)filter name:(NSString*)name minX:(CGFloat)minX maxX:(CGFloat)maxX minY:(CGFloat)minY maxY:(CGFloat)maxY;
- (id)initWithFilterOne:(GPUImageOutput<GPUImageInput>*)filterOne filterTwo:(GPUImageOutput<GPUImageInput>*)filterTwo name:(NSString*)name minX:(CGFloat)minX maxX:(CGFloat)maxX minY:(CGFloat)minY maxY:(CGFloat)maxY;

@end
