//
//  CLEProcessedImageView.m
//  PhotoEdit
//
//  Created by Christian Lee on 18/10/13.
//  Copyright (c) 2013 Clee. All rights reserved.
//

#import "CLEProcessedImageView.h"
#import "CLEGroup.h"

@implementation CLEProcessedImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		self.primaryView = [[GPUImageView alloc] initWithFrame: CGRectMake(0, 0, frame.size.width, frame.size.height)];
		[self addSubview:self.primaryView];
    }
    return self;
}

- (void)createFiltersForImage:(UIImage*)image withSize:(CGSize)size
{
	self.sourcePicture = [[GPUImagePicture alloc] initWithImage:image smoothlyScaleOutput:YES];
	
	// Groups
	self.groups = [[NSMutableDictionary alloc] init];
	
	// Bright
	CLEGroup *group = [[CLEGroup alloc] init];
	group.name = @"Bright";
	group.effects = [[NSArray alloc] initWithObjects:
					 [NSNumber numberWithInt:CLEEffectLibraryEffectBrightnessContrast],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectExposure],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectGamma],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectHighlightShadow],
					 nil];
	[self.groups setObject:group forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectGroupBright]];
	
	// Colour
	group = [[CLEGroup alloc] init];
	group.name = @"Color";
	group.effects = [[NSArray alloc] initWithObjects:
					 [NSNumber numberWithInt:CLEEffectLibraryEffectSaturation],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectHue],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectRgbRed],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectRgbGreen],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectRgbBlue],
					 //[NSNumber numberWithInt:CLEEffectLibraryEffectInvert],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectWhiteBalance],
					 nil];
	[self.groups setObject:group
					forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectGroupColour]];
	
	// Sharpen
	group = [[CLEGroup alloc] init];
	group.name = @"Sharpen";
	group.effects = [[NSArray alloc] initWithObjects:
					 [NSNumber numberWithInt:CLEEffectLibraryEffectSharp],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectNoise],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectGaussian],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectMotionBlur],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectZoomBlur],
					 nil];
	[self.groups setObject:group
					forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectGroupSharpen]];
	
	// Stylize
	group = [[CLEGroup alloc] init];
	group.name = @"Stylize";
	group.effects = [[NSArray alloc] initWithObjects:
					 [NSNumber numberWithInt:CLEEffectLibraryEffectPixellate],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectPolkadot],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectHalftone],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectHaze],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectVignette],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectLineGenerator],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectCrosshatch],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectSketch],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectToon],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectEmboss],
					 [NSNumber numberWithInt:CLEEffectLibraryEffectMosaic],
					 nil];
	[self.groups setObject:group
					forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectGroupStylize]];
	
	// Filters
	self.filters = [[NSMutableDictionary alloc] init];
	
	//	// GaussianBlur - NB very slow
	//	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageGaussianBlurFilter alloc] init] name:@"Gaussian Blur" minX:0.0 maxX:1.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectGaussian]];
	
	// MotionBlur
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageMotionBlurFilter alloc] init] name:@"Motion Blur" minX:0.0 maxX:10.0 minY:0.0 maxY:360.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectMotionBlur]];
	
	// Pixellate
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImagePixellateFilter alloc] init] name:@"Pixellate" minX:0.0 maxX:0.05] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectPixellate]];
	
	// PolkaDot
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImagePolkaDotFilter alloc] init] name:@"PolkaDot" minX:0.0 maxX:0.045 minY:0.5 maxY:1.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectPolkadot]];
	
	// Halftone
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageHalftoneFilter alloc] init] name:@"Halftone" minX:0.001 maxX:0.025] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectHalftone]];
	
	// Haze
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageHazeFilter alloc] init] name:@"Haze" minX:-0.3 maxX:0.3 minY:-0.3 maxY:0.3] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectHaze]];
	
	/*
	 
	 Sketch
	 Emboss
	 Mosaic
	 */
	
	// Vignette
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageVignetteFilter alloc] init] name:@"Vignette" minX:0.1 maxX:0.7] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectVignette]];
	
	// RGB (Red)
	GPUImageRGBFilter *rgbFilter = [[GPUImageRGBFilter alloc] init];
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:rgbFilter name:@"Red Channel" minX:0.0 maxX:2.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectRgbRed]];
	
	// RGB (Blue)
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:rgbFilter name:@"Green Channel" minX:0.0 maxX:2.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectRgbGreen]];
	
	// RGB (Green)
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:rgbFilter name:@"Blue Channel" minX:0.0 maxX:2.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectRgbBlue]];
	
	
	// LineGenerator
	// [self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageLineGenerator alloc] init] name:@"Line" minX:0.0 maxX:1.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectLineGenerator]];
	
	// ZoomBlur
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageZoomBlurFilter alloc] init] name:@"Zoom Blur" minX:0.0 maxX:3.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectZoomBlur]];
	
	// Invert
	// [self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageInver alloc] init] name:@"" minX:0.0 maxX:1.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectInvert]];
	
	// Crosshatch
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageCrosshatchFilter alloc] init] name:@"Crosshatch" minX:0.01 maxX:0.05 minY:0.01 maxY:0.045] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectCrosshatch]];
	
	// Toon
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageToonFilter alloc] init] name:@"Toon" minX:0.0 maxX:1.0 minY:0.0 maxY:100.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectToon]];
	
	/*
	 //
	 [self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImage alloc] init] name:@"" minX:0.0 maxX:1.0] forKey:[NSNumber numberWithInt:E_]];
	 
	 //
	 [self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImage alloc] init] name:@"" minX:0.0 maxX:1.0] forKey:[NSNumber numberWithInt:E_]];
	 
	 //
	 [self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImage alloc] init] name:@"" minX:0.0 maxX:1.0] forKey:[NSNumber numberWithInt:E_]];
	 */
	
	// Add self.filters
	[self.filters setObject: [[CLEEffect alloc] initWithFilterOne:[[GPUImageBrightnessFilter alloc] init] filterTwo:[[GPUImageContrastFilter alloc] init] name:@"Brightness / Contrast" minX:-0.9 maxX:0.9 minY:0.0 maxY:2.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectBrightnessContrast]];
	
	[(GPUImageBrightnessFilter*)((CLEEffect*)[self.filters objectForKey:[NSNumber numberWithInt:CLEEffectLibraryEffectBrightnessContrast]]).filterOne forceProcessingAtSize:size]; // Default value for hue is incorrect?
	
	// Exposure
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageExposureFilter alloc] init] name:@"Exposure" minX:-3.5 maxX:3.5] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectExposure]];
	
	// Saturation
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageSaturationFilter alloc] init] name:@"Saturation" minX:0.0 maxX:2.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectSaturation]];
	
	// Gamma
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageGammaFilter alloc] init] name:@"Gamma" minX:0.1 maxX:3.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectGamma]];
	
	// Hue
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageHueFilter alloc] init] name:@"Hue" minX:0.0 maxX:180.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectHue]];
	[(GPUImageHueFilter*)((CLEEffect*)[self.filters objectForKey:[NSNumber numberWithInt:CLEEffectLibraryEffectHue]]).filterOne setHue:0.0]; // Default value for hue is incorrect?
	
	// Highlight / shadow
	[self.filters setObject:[[CLEEffect alloc] initWithFilterOne:[[GPUImageHighlightShadowFilter alloc] init] filterTwo:nil name:@"Hightlight / Shadow" minX:0.0 maxX:1.0 minY:0.0 maxY:1.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectHighlightShadow]];
	
	// Sharpen
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageSharpenFilter alloc] init] name:@"Sharpen" minX:0.0 maxX:4.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectSharp]];
	
	// White balance / temperature
	[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageWhiteBalanceFilter alloc] init] name:@"Temperature" minX:2000.0 maxX:20000.0 minY:-150.0 maxY:150.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectWhiteBalance]];
	
	// Doesn't work?
	
	// Levels
	//[self.filters setObject:[[CLEEffect alloc] initWithFilterOne:[[GPUImageLevelsFilter alloc] init] filterTwo:nil name:@"Levels" minX:0.0 maxX:1.0 minY:0.0 maxY:1.0] forKey:[NSNumber numberWithInt:E_LEVELS]];
	
	
	//[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImageUnsharpMaskFilter alloc] init] name:@"Unsharp" minX:0.0 maxX:1.0 minY:0.0 maxY:1.0] forKey:[NSNumber numberWithInt:E_UNSHARP]];
	
	//[self.filters setObject:[[CLEEffect alloc] initWithFilter:[[GPUImagePerlinNoiseFilter alloc] init] name:@"Noise" minX:0.0 maxX:1.0] forKey:[NSNumber numberWithInt:CLEEffectLibraryEffectNoise]];
	
	//
}

- (void)updateFilterAtIndex:(int)effectIndex withXPos:(CGFloat)xPos yPos:(CGFloat)yPos
{
	CLEEffect *effect = [self.filters objectForKey:[NSNumber numberWithInt:effectIndex]];
	
	// Flip y Value (so it goes "bottom to top" or "left to right)
	yPos = 1 - yPos;
	
//	NSLog(@"X: %.3f", xPos);
//	NSLog(@"Y: %.3f", yPos);
	
	// Set min and max
	CGFloat minX = effect.minX;
	CGFloat maxX = effect.maxX;
	
	CGFloat minY = effect.minY;
	CGFloat maxY = effect.maxY;
	
	// Set x and y values within range
	CGFloat xValue = minX + xPos * (maxX - minX);
	CGFloat yValue = minY + yPos * (maxY - minY);
	
//	NSLog(@"X val: %.3f", xValue);
//	NSLog(@"Y val: %.3f", yValue);
	
	// Apply filter
	switch (effectIndex)
	{
		case CLEEffectLibraryEffectBrightnessContrast:
		{
			[(GPUImageBrightnessFilter *)effect.filterOne setBrightness:xValue];
			[(GPUImageContrastFilter *)effect.filterTwo setContrast:yValue];
			break;
		}
		case CLEEffectLibraryEffectExposure:
		{
			[(GPUImageExposureFilter *)effect.filterOne setExposure:xValue];
			break;
		}
		case CLEEffectLibraryEffectSaturation:
		{
			[(GPUImageSaturationFilter *)effect.filterOne setSaturation:xValue];
			break;
		}
		case CLEEffectLibraryEffectGamma:
		{
			[(GPUImageGammaFilter *)effect.filterOne setGamma: effect.maxX - xValue + effect.minX];
			break;
		}
		case CLEEffectLibraryEffectHue:
		{
			[(GPUImageHueFilter *)effect.filterOne setHue:xValue];
			break;
		}
		case CLEEffectLibraryEffectHighlightShadow:
		{
			[(GPUImageHighlightShadowFilter *)effect.filterOne setHighlights:xValue];
			[(GPUImageHighlightShadowFilter *)effect.filterOne setShadows:yValue];
			break;
		}
		case CLEEffectLibraryEffectSharp:
		{
			[(GPUImageSharpenFilter *)effect.filterOne setSharpness:xValue];
			break;
		}
		case CLEEffectLibraryEffectNoise:
		{
			[(GPUImagePerlinNoiseFilter *)effect.filterOne setScale:xValue];
			break;
		}
		case CLEEffectLibraryEffectGaussian:
		{
			[(GPUImageGaussianBlurFilter *)effect.filterOne setBlurSize:xValue];
			break;
		}
		case CLEEffectLibraryEffectMotionBlur:
		{
			[(GPUImageMotionBlurFilter *)effect.filterOne setBlurSize:xValue];
			[(GPUImageMotionBlurFilter *)effect.filterOne setBlurAngle:yValue];
			break;
		}
		case CLEEffectLibraryEffectPixellate:
		{
			[(GPUImagePixellateFilter *)effect.filterOne setFractionalWidthOfAPixel:xValue];
			break;
		}
		case CLEEffectLibraryEffectPolkadot:
		{
			[(GPUImagePolkaDotFilter *)effect.filterOne setFractionalWidthOfAPixel:xValue];
			[(GPUImagePolkaDotFilter *)effect.filterOne setDotScaling:yValue];
			break;
		}
		case CLEEffectLibraryEffectHalftone:
		{
			[(GPUImageHalftoneFilter *)effect.filterOne setFractionalWidthOfAPixel:xValue];
			break;
		}
		case CLEEffectLibraryEffectHaze:
		{
			[(GPUImageHazeFilter *)effect.filterOne setDistance:xValue];
			[(GPUImageHazeFilter *)effect.filterOne setSlope:yValue];
			break;
		}
		case CLEEffectLibraryEffectVignette:
		{
			[(GPUImageVignetteFilter *)effect.filterOne setVignetteStart:xValue];
			break;
		}
		case CLEEffectLibraryEffectRgbRed:
		{
			[(GPUImageRGBFilter *)effect.filterOne setRed:xValue];
			break;
		}
		case CLEEffectLibraryEffectRgbGreen:
		{
			[(GPUImageRGBFilter *)effect.filterOne setGreen:xValue];
			break;
		}
		case CLEEffectLibraryEffectRgbBlue:
		{
			[(GPUImageRGBFilter *)effect.filterOne setBlue:xValue];
			break;
		}
		case CLEEffectLibraryEffectZoomBlur:
		{
			[(GPUImageZoomBlurFilter *)effect.filterOne setBlurSize:xValue];
			break;
		}
		case CLEEffectLibraryEffectCrosshatch:
		{
			[(GPUImageCrosshatchFilter *)effect.filterOne setCrossHatchSpacing:xValue];
			[(GPUImageCrosshatchFilter *)effect.filterOne setLineWidth:yValue];
			break;
		}
		case CLEEffectLibraryEffectToon:
		{
			[(GPUImageToonFilter *)effect.filterOne setThreshold:xValue];
			[(GPUImageToonFilter *)effect.filterOne setQuantizationLevels:yValue];
			break;
		}
		case CLEEffectLibraryEffectWhiteBalance:
		{
			[(GPUImageWhiteBalanceFilter *)effect.filterOne setTemperature:xValue];
			[(GPUImageWhiteBalanceFilter *)effect.filterOne setTint:yValue];
			break;
		}
			/*
			 case CLEEffectLibraryEffect:
			 {
			 [(GPUImage *)effect.filterOne set:xValue];
			 break;
			 }
			 */
		default:
			break;
	}
	
	[self processImage];
}

- (void)filterImage
{
	GPUImageOutput *previousFilter = self.sourcePicture;
	for (GPUImageOutput<GPUImageInput> *currentFilter in self.currentFilters) {
		[previousFilter removeAllTargets];
		[previousFilter addTarget:currentFilter];
		previousFilter = currentFilter;
	}
	
	[previousFilter addTarget:self.primaryView];
	[self processImage];
}

- (void)processImage
{
	// Make sure we don't call process image if it is still processing
	if (!processing)
	{
		processing = YES;
		[self.sourcePicture processImageWithCompletionHandler:^{
			processing = NO;
		}];
	}
}

- (void)startFiltersForGroup
{
	// Set filters
	self.currentFilters = [NSMutableArray arrayWithArray: self.usedFilters];
}

- (void)applyFiltersForGroup
{
	// Set filters
	self.usedFilters = [NSMutableArray arrayWithArray: self.currentFilters];
	self.currentFilters = nil;
}

- (void)saveImageToCameraRoll
{
	UIImage *processedImage = [(GPUImageOutput*)[self.usedFilters lastObject] imageFromCurrentlyProcessedOutput];
	UIImageWriteToSavedPhotosAlbum(processedImage, nil, nil, nil);
}

- (void)addEffect:(CLEEffect*)effect
{
	if ([self.currentFilters indexOfObject:effect.filterOne] == NSNotFound)
	{
		[self.currentFilters addObject:effect.filterOne];
	}
	if (effect.filterTwo)
	{
		if ([self.currentFilters indexOfObject:effect.filterTwo] == NSNotFound)
		{
			[self.currentFilters addObject:effect.filterTwo];
		}
	}
	
	[self filterImage];
}

- (void)removeFilters
{
	self.usedFilters = [[NSMutableArray alloc] init];
	[self filterImage];
}

@end
