//
//  main.m
//  CameraBlack
//
//  Created by Christian Lee on 19/09/13.
//  Copyright (c) 2013 Clee. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CLEAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([CLEAppDelegate class]));
	}
}
