#import "CLEPhotoEditViewController.h"
#import "CLEGroup.h"
#import "CLEEffect.h"

@implementation CLEPhotoEditViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	// Hide nav controller bar
	[self.navigationController setNavigationBarHidden:YES];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	if (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown)
	{
		return YES;
	}
	return NO;
}

#pragma mark - View lifecycle

- (void)loadView
{
	// Testing - set default image if none found
	if (self.image == nil)
	{
		//self.image = [UIImage imageNamed:@"city.jpeg"];
		self.image = [UIImage imageNamed:@"leahss.jpg"];
	}
	
//	// TODO this could be better
//	if (self.image.size.width > self.image.size.height)
//	{
//		self.image = [UIImage imageWithCGImage:self.image.CGImage scale:self.image.scale orientation:UIImageOrientationRight];
//	}
	
	// Create view
	self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
	self.view.backgroundColor = [UIColor colorWithRed:0.968628 green:0.952941 blue:0.941177 alpha:1];
	
	// Create image processing view
	CGRect imageFrame = CGRectMake(self.view.frame.origin.x,
								   (self.view.frame.size.height - self.image.size.height / self.image.size.width * self.view.frame.size.width) / 2,
								   self.view.frame.size.width,
								   self.image.size.height / self.image.size.width * self.view.frame.size.width);
	
	self.processedImageView = [[CLEProcessedImageView alloc] initWithFrame:imageFrame];
	[self.view addSubview:self.processedImageView];
	
	// Create filters
	[self setupDisplayFiltering];
	
	// Create sliders
	xSlider = [[UISlider alloc] initWithFrame:CGRectMake(60.0, 488, 200.0, 10.0)];
	[xSlider setBackgroundColor:[UIColor clearColor]];
	xSlider.minimumValue = 0.0;
	xSlider.maximumValue = 1.0;
	xSlider.continuous = YES;
	xSlider.value = 0.5;
	xSlider.hidden = YES;
	[self.view addSubview:xSlider];
	
	[xSlider addTarget:self action:@selector(sliderValueChanged:)
	   forControlEvents:UIControlEventValueChanged];
	
	ySlider = [[UISlider alloc] initWithFrame:CGRectMake(-80, 260.0, 200.0, 10.0)];
	[ySlider setBackgroundColor:[UIColor clearColor]];
	ySlider.minimumValue = 0.0;
	ySlider.maximumValue = 1.0;
	ySlider.continuous = YES;
	ySlider.value = 0.5;
	ySlider.hidden = YES;
	ySlider.transform = CGAffineTransformMakeRotation(M_PI * 0.5);
	[self.view addSubview:ySlider];
	
	[ySlider addTarget:self action:@selector(sliderValueChanged:)
	  forControlEvents:UIControlEventValueChanged];
	
	// Create effects list toolbar
	self.effectView = [[UIScrollView alloc] init];
	self.effectView.frame = CGRectMake(0, 0, 320, 40);
	self.effectView.pagingEnabled = NO;
	self.effectView.clipsToBounds = NO;
	self.effectView.showsHorizontalScrollIndicator = NO;
	self.effectView.backgroundColor = [UIColor clearColor];
	
	// Add translucency behind effect buttons
	self.effectToolbar = [[UIToolbar alloc] init];
	[self.effectToolbar setTranslatesAutoresizingMaskIntoConstraints:NO];
	self.effectToolbar.barStyle = UIBarStyleDefault;
	[self.view addSubview:self.effectToolbar];
	[self.effectToolbar addSubview:self.effectView];
	
	// Create Effect buttons
	[self populateGroupList];
	
	// Create 'Edit' toolbar
	self.editToolbar = [[UIToolbar alloc] init];
	[self.editToolbar setTranslatesAutoresizingMaskIntoConstraints:NO];
	self.editToolbar.barStyle = UIBarStyleDefault;
	NSMutableArray *items = [[NSMutableArray alloc] init];
	[items addObject:[[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPressed:)]];
	[items addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
	self.saveAcceptItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveImagePressed:)];
	[items addObject:self.saveAcceptItem];
	[self.editToolbar setItems:items animated:NO];
	[self.view addSubview:self.editToolbar];
	
	// Layout
	
	// Edit toolbar
	[self.view addConstraint: [NSLayoutConstraint constraintWithItem:self.editToolbar attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
	[self.view addConstraint: [NSLayoutConstraint constraintWithItem:self.editToolbar attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0]];
	[self.view addConstraint: [NSLayoutConstraint constraintWithItem:self.editToolbar attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
	
	// Effects
	UIView *effectToolbar = self.effectToolbar;
	UIView *effectView = self.effectView;
	NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(effectToolbar, effectView);
	[self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"|[effectToolbar]|" options:NSLayoutFormatAlignAllBaseline metrics:nil
																		 views:viewsDictionary]];
	[self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[effectToolbar]|" options:0 metrics:nil
																		 views:viewsDictionary]];
	
	// Interaction
	UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTap:)];
    [imageTap setNumberOfTapsRequired:1];
	imageTap.delegate = self.processedImageView;
    [self.processedImageView addGestureRecognizer:imageTap];
}

- (void)populateGroupList
{
	CGFloat xPos = 40;
	[self.effectView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
	
	for (NSNumber *key in self.processedImageView.groups.allKeys)
	{
		CLEGroup *group = [self.processedImageView.groups objectForKey:key];
		[self.processedImageView.filters objectForKey:key];
		UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
		button.tag = [key intValue];
		[button addTarget:self
				   action:@selector(groupButtonPressed:)
		 forControlEvents:UIControlEventTouchDown];
		[button setTitle:group.name forState:UIControlStateNormal];
		CGSize stringSize = [group.name sizeWithFont:[UIFont systemFontOfSize:18]];
		button.frame = CGRectMake(xPos, 0, stringSize.width, 40.0);
		[self.effectView addSubview:button];
		xPos += stringSize.width + 10;
	}
	
	// Set effect view content size
	self.effectView.contentSize = CGSizeMake(xPos, 40);
	
	self.saveAcceptItem.title = @"Save";
}

- (void)populateEffectsListForGroup:(int)groupIndex
{
	CGFloat xPos = 0;
	[self.effectView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
	
	CLEGroup *group = [self.processedImageView.groups objectForKey:[NSNumber numberWithInt:groupIndex]];
	
	for (NSNumber *key in group.effects)
	{
		CLEEffect *effect = [self.processedImageView.filters objectForKey:key];
		UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
		button.tag = [key intValue];
		[button addTarget:self
				   action:@selector(effectButtonPressed:)
		 forControlEvents:UIControlEventTouchDown];
		[button setTitle:effect.name forState:UIControlStateNormal];
		CGSize stringSize = [effect.name sizeWithFont:[UIFont systemFontOfSize:18]];
		button.frame = CGRectMake(xPos, 0, stringSize.width, 40.0);
		[self.effectView addSubview:button];
		xPos += stringSize.width + 10;
	}
	
	// Set effect view content size
	self.effectView.contentSize = CGSizeMake(xPos, 40);
	
	// Change 'save' to 'apply'
	self.saveAcceptItem.title = @"Apply";
	
	[self.processedImageView startFiltersForGroup];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	
    // Disallow recognition of tap gestures in the segmented control.
    if (touch.view != self.view && touch.view != self.processedImageView) {//change it to your condition
        return NO;
    }
    return YES;
}

- (void)imageTap:(UITapGestureRecognizer *)gestureRecognizer
{
	// Show / Hide interface on tap
	self.editToolbar.hidden = !self.editToolbar.hidden;
	self.effectToolbar.hidden = self.editToolbar.hidden;
	xSlider.hidden = self.editToolbar.hidden;
	ySlider.hidden = self.editToolbar.hidden;
}

- (void)saveImagePressed:(UIButton*)button
{
	if ([self.saveAcceptItem.title isEqualToString:@"Save"])
	{
		button.enabled = NO;
		
		// Save to camera roll
		[self.processedImageView saveImageToCameraRoll];
		
		// Go back to camera
		[self.navigationController popViewControllerAnimated:YES];
	}
	else
	{
		// Save current filters to used filters
		[self.processedImageView applyFiltersForGroup];
		
		// Close filter group
		[self populateGroupList];
	}
}

- (void)cancelPressed:(UIButton*)button
{
	// Go back to camera
	if ([self.saveAcceptItem.title isEqualToString:@"Save"])
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
	else
	{
		// Close filter group
		[self populateGroupList];
		
		[self.processedImageView startFiltersForGroup];
		[self.processedImageView filterImage];
		
		// Hide sliders
		xSlider.hidden = YES;
		ySlider.hidden = YES;
	}
}

- (void)resetPressed:(UIButton*)button
{
	// Go back to camera
	[self setupDisplayFiltering];
}

- (void)groupButtonPressed:(UIButton*)button
{
	[self populateEffectsListForGroup:button.tag];
}

- (void)effectButtonPressed:(UIButton*)button
{
	currentEffectIndex = button.tag;
	
	// Select button
	if (selectedButton)
	{
		selectedButton.selected = NO;
	}
	selectedButton = button;
	button.selected = YES;
	
	// Center the button
	CGFloat newX = button.frame.origin.x - (self.effectView.frame.size.width - button.frame.size.width) / 2;
	newX = newX > 0 ? newX : 0;
	[self.effectView setContentOffset:CGPointMake(newX, 0) animated:YES];
	
	// Add effect to list
	CLEEffect *effect = [self.processedImageView.filters objectForKey:[NSNumber numberWithInt:currentEffectIndex]];
	
	// Show / hide sliders
	xSlider.hidden = NO;
	ySlider.hidden = effect.maxY - effect.minY == 0;
	
	[self.processedImageView addEffect:effect];
}

#pragma mark -
#pragma mark Image filtering

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
	UITouch *touch = [touches anyObject];
	CGPoint currentTouch = [touch locationInView:self.view];
	
	CGFloat xPos = currentTouch.x / 320;
	CGFloat yPos = currentTouch.y / 400;
	
	// Update sliders
	xSlider.value = xPos;
	ySlider.value = yPos;
	
	[self updateEffect];
}

- (IBAction)sliderValueChanged:(UISlider *)sender
{
	[self updateEffect];
}

- (void)updateEffect
{
	CGFloat xPos = xSlider.value;
	CGFloat yPos = ySlider.value;
	
	[self.processedImageView updateFilterAtIndex:currentEffectIndex withXPos:xPos yPos:yPos];
}

- (void)setupDisplayFiltering;
{
	UIImage *inputImage = self.image;
	
	// Rotate image to correct orientation (as GPUImagePicture ignores the orientation)
	// TODO use raw image when saving, and save orientation to new image
	if (inputImage.imageOrientation != UIImageOrientationUp)
	{
		UIGraphicsBeginImageContextWithOptions(inputImage.size, NO, inputImage.scale);
		[inputImage drawInRect:(CGRect){0, 0, inputImage.size}];
		UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		inputImage = normalizedImage;
	}
	
	[self.processedImageView createFiltersForImage:inputImage withSize:self.processedImageView.primaryView.sizeInPixels];
	
	// Remove existing filters
	[self.processedImageView removeFilters];
}

@end
