//
//  CLEGroup.h
//  PhotoEdit
//
//  Created by Christian Lee on 17/10/13.
//  Copyright (c) 2013 Clee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLEGroup : NSObject

@property (strong) NSString *name;
@property (strong) NSArray *effects;

@end
