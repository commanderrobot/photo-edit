#import "CLEChoosePhotoController.h"
#import "CLEPhotoEditViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "CLEBlackAndWhiteFilter.h"

@interface CLEChoosePhotoController ()

@end

@implementation CLEChoosePhotoController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView 
{
	[super loadView];
	
	//self.view = [[UIView alloc] initWithFrame:mainScreenFrame];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.view.backgroundColor = [UIColor colorWithRed:0.968628 green:0.952941 blue:0.941177 alpha:1];
	
	// Icon View
	UIImageView *iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"FrontIcon.png"]];
	[iconImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
	[self.view addSubview:iconImageView];
	
	// Photo capture button
	UIButton *galleryButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[galleryButton setTranslatesAutoresizingMaskIntoConstraints:NO];
	[galleryButton setTitle:@"Choose Photo" forState:UIControlStateNormal];
	galleryButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
	[galleryButton addTarget:self action:@selector(selectPhoto:) forControlEvents:UIControlEventTouchUpInside];
	[galleryButton setTitleColor:[UIColor blueColor] forState:UIControlStateDisabled];
	[galleryButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
	[self.view addSubview:galleryButton];
	
	// Photo capture button
	UIButton *photoCaptureButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[photoCaptureButton setTranslatesAutoresizingMaskIntoConstraints:NO];
	[photoCaptureButton setTitle:@"Take Photo" forState:UIControlStateNormal];
	photoCaptureButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
	[photoCaptureButton addTarget:self action:@selector(takePhoto:) forControlEvents:UIControlEventTouchUpInside];
	[photoCaptureButton setTitleColor:[UIColor blueColor] forState:UIControlStateDisabled];
	[photoCaptureButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
	[self.view addSubview:photoCaptureButton];
	
	// Layout
	
	// Icon view
	[self.view addConstraint: [NSLayoutConstraint constraintWithItem:iconImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
	
	NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(iconImageView, galleryButton, photoCaptureButton);
	
	[self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"|-[photoCaptureButton]-|" options:NSLayoutFormatAlignAllBaseline metrics:nil
								views:viewsDictionary]];
	[self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"|-[galleryButton]-|" options:NSLayoutFormatAlignAllBaseline metrics:nil
								views:viewsDictionary]];
	[self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[photoCaptureButton]-[galleryButton]-200-|" options:0 metrics:nil
							   views:viewsDictionary]];
	
	[self.view addConstraint: [NSLayoutConstraint constraintWithItem:iconImageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:photoCaptureButton attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
	
	// Testing
	hasShownTest = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
	// Testing - show photo editor immediately
	if (!hasShownTest)
	{
		[self editImage:nil];
		hasShownTest = YES;
	}
	
	// Hide navigation bar
	[self.navigationController setNavigationBarHidden:YES animated:animated];
	
	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
	
	[super viewWillAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)selectPhoto:(UIButton *)sender
{
	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	//picker.allowsEditing = YES;
	picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

	[self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)takePhoto:(UIButton *)sender
{
	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.sourceType = UIImagePickerControllerSourceTypeCamera;
	
	[self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	//UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
	UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
	
	[self editImage:chosenImage];
	
	[picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)editImage:(UIImage*)image
{
	CLEPhotoEditViewController *simpleImageViewController = [[CLEPhotoEditViewController alloc] initWithNibName:nil bundle:nil];
	simpleImageViewController.image = image;
	[self.navigationController pushViewController:simpleImageViewController animated:YES];
}

@end