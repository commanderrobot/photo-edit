//
//  CLEAppDelegate.h
//  CameraBlack
//
//  Created by Christian Lee on 19/09/13.
//  Copyright (c) 2013 Clee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLEAppDelegate : UIResponder <UIApplicationDelegate>
{
	
	UIViewController *rootViewController;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;

@end
