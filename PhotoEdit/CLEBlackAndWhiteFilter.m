#import "CLEBlackAndWhiteFilter.h"

@implementation CLEBlackAndWhiteFilter

- (id)init;
{
    if (!(self = [super init]))
    {
		return nil;
    }
    
    self.intensity = 1.0;

	self.colorMatrix = (GPUMatrix4x4){
        {0.299, 0.587, 0.114, 0.0},
        {0.299, 0.587, 0.114, 0.0},
        {0.299, 0.587, 0.114 ,0.0},
        {0,0,0,1.0},
    };

    return self;
}

@end

