#import <UIKit/UIKit.h>
#import "CLEProcessedImageView.h"

@interface CLEPhotoEditViewController : UIViewController <UIGestureRecognizerDelegate>
{
	int currentEffectIndex;
	UISlider *xSlider;
	UISlider *ySlider;
	UIButton *selectedButton;
}

@property (nonatomic, strong) UIToolbar *groupToolbar;
@property (nonatomic, strong) UIToolbar *effectToolbar;
@property (nonatomic, strong) UIToolbar *editToolbar;
@property (nonatomic, strong) UIBarButtonItem *cancelItem;
@property (nonatomic, strong) UIBarButtonItem *saveAcceptItem;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIScrollView *effectView;
@property (nonatomic, strong) CLEProcessedImageView *processedImageView;

@end
