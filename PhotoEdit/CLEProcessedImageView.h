//
//  CLEProcessedImageView.h
//  PhotoEdit
//
//  Created by Christian Lee on 18/10/13.
//  Copyright (c) 2013 Clee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"
#import "CLEEffect.h"

#define G_BRIGHT 0
#define G_COLOUR 1
#define G_SHARPEN 2
#define G_STYLIZE 3

typedef NS_ENUM(NSInteger, CLEEffectLibraryEffectGroup) {
	CLEEffectLibraryEffectGroupBright,
	CLEEffectLibraryEffectGroupColour,
	CLEEffectLibraryEffectGroupSharpen,
	CLEEffectLibraryEffectGroupStylize,
};

typedef NS_ENUM(NSInteger, CLEEffectLibraryEffectEffect) {
	CLEEffectLibraryEffectBrightnessContrast,
	CLEEffectLibraryEffectExposure,
	CLEEffectLibraryEffectSaturation,
	CLEEffectLibraryEffectGamma,
	CLEEffectLibraryEffectHue,
	CLEEffectLibraryEffectHighlightShadow,
	CLEEffectLibraryEffectSharp,
	CLEEffectLibraryEffectNoise,
	CLEEffectLibraryEffectGaussian,
	CLEEffectLibraryEffectMotionBlur,
	CLEEffectLibraryEffectPixellate,
	CLEEffectLibraryEffectPolkadot,
	CLEEffectLibraryEffectHalftone,
	CLEEffectLibraryEffectHaze,
	CLEEffectLibraryEffectVignette,
	CLEEffectLibraryEffectRgbRed,
	CLEEffectLibraryEffectRgbBlue,
	CLEEffectLibraryEffectRgbGreen,
	CLEEffectLibraryEffectLineGenerator,
	CLEEffectLibraryEffectZoomBlur,
	CLEEffectLibraryEffectInvert,
	CLEEffectLibraryEffectCrosshatch,
	CLEEffectLibraryEffectSketch,
	CLEEffectLibraryEffectToon,
	CLEEffectLibraryEffectEmboss,
	CLEEffectLibraryEffectMosaic,
	CLEEffectLibraryEffectWhiteBalance,
};

//#define E_UNSHARP 7
//#define E_LEVELS 8

@interface CLEProcessedImageView : UIView<UIGestureRecognizerDelegate> {
	BOOL processing;
}

@property (nonatomic, strong) GPUImageView *primaryView;
@property (nonatomic, strong) GPUImagePicture *sourcePicture;
@property (nonatomic, strong) NSMutableDictionary *groups;
@property (nonatomic, strong) NSMutableDictionary *filters;
@property (nonatomic, strong) NSMutableArray *currentFilters;
@property (nonatomic, strong) NSMutableArray *usedFilters;

- (void)createFiltersForImage:(UIImage*)image withSize:(CGSize)size;
- (void)updateFilterAtIndex:(int)effectIndex withXPos:(CGFloat)xPos yPos:(CGFloat)yPos;
- (void)filterImage;
- (void)startFiltersForGroup;
- (void)applyFiltersForGroup;
- (void)saveImageToCameraRoll;
- (void)addEffect:(CLEEffect*)effect;
- (void)removeFilters;

@end
